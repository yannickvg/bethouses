# betshops

## Description

This app was developed as a technical assignment during the recruitment process for a customer I am currently working for, to show my Android skills.
The codebase is not so big, since the task was timeboxed.

These are the original requirements:

* Application shows betshops for client on map (using default mapping library for platform) -> bet shops are located in Germany
* User can move the map and zoom-in/zoom-out
* When clicking on betshop icon, detail pops out and user can see the relevant information about the betshop
* When clicking on the "Route" button, user is transferred into default navigation application to navigate to betshop location

The design was also delivered with the assignment instructions.

## Technical Info

### How to run

To make it easy to try the app out, an apk can be found in the release directory. If you open this apk on an android device, it will ask to install the app.

To run this app from the codebase, you need to have the Android SDK installed and have a valid Google Maps API key. The easiest way is to install Android Studio, which will also install the sdk with a standard installation.
If you have a API key, put it in a file `local.properties` in the root of the project like this `MAPS_API_KEY=<your key>`

### Architecture / Libraries

Until some time ago, the Android team never really expressed an opinion on what a good app architecture meant for them. With the release of all jetpack libraries this has changed.
When I developed this assignment, I took it as an opportunity to try out some of the architecture libraries and guidelines that the Android team has made recently.
This is why a lot of the libraries used in this codebase are fairly new.

For now the app has a data layer with the code to talk to the REST api and a repository.
The app doesn't have much business logic, which is why there are no services or usecases.

The view part of the app is build with viewmodels and livedata. By using this, the view observes the data in the viewmodel and gets updated automatically, based on the state in the viewmodel.
I personally really like this principle because it leads to clean UI code with not too much boilerplate.

### Improvements / Missing

Since the task was timeboxed, some things are missing that I would add in a normal project.

- Unit tests: added tests was outside of the scope of the assignment.
In a real project I would at least make sure the the business logic is covered by unit tests so that breaking changes are discovered in the build process.
- UI tests: In the past, running UI tests for android apps was very maintenance heavy because it was hard to get these tests stable and keep them stable.
Therefore, UI tests is not something that I add by default for Android projects, but maybe stability on this part has improved recently.
- CI/CD: For  me, almost every project needs a build pipeline to automate basic tasks and distribute test versions to stakeholders.
- Support for multiple languages + fetch these from a source so labels can be externally managed.
- ...




