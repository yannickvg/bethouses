package be.rmdy.betshops.betshopmap.data.model

import be.rmdy.betshops.betshopmap.model.Betshop

data class ApiBetshop(val name:String,
                      val location: ApiLocation,
                      val id: Long,
                      val county: String,
                      val cityId: Long,
                      val city: String,
                      val address: String)

fun ApiBetshop.toBetshop() : Betshop {
    return Betshop(
        name,
        location.lat,
        location.lng,
        id,
        county,
        city,
        address
    )
}

