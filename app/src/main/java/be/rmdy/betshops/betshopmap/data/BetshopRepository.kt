package be.rmdy.betshops.betshopmap.data

import be.rmdy.betshops.betshopmap.data.model.toBetshop
import be.rmdy.betshops.betshopmap.model.Betshop
import javax.inject.Inject

class BetshopRepository @Inject constructor(private val betshopApi: BetshopApi) {

    suspend fun getBetshops(boundingBox: String): List<Betshop> {
        val response = betshopApi.getBetshops(boundingBox)
        return if (response.isSuccessful) {
            response.body()?.betshops?.map { it.toBetshop() } ?: emptyList()
        } else {
            emptyList()
        }
    }

}