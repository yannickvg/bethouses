package be.rmdy.betshops.betshopmap.view

import be.rmdy.betshops.betshopmap.model.Betshop
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class ClusteredBetshop(
    private val betshop: Betshop,
    val isSelected: Boolean
) : ClusterItem {

    override fun getPosition(): LatLng {
        return LatLng(betshop.latitude, betshop.longitude)
    }

    override fun getTitle(): String {
        return betshop.name
    }

    override fun getSnippet(): String? {
        return ""
    }

    fun getId(): Long {
        return betshop.id
    }
}