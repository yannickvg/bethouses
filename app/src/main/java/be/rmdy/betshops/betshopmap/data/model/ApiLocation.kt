package be.rmdy.betshops.betshopmap.data.model

data class ApiLocation(val lat: Double, val lng: Double)