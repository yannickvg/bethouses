package be.rmdy.betshops.betshopmap.model

import be.rmdy.betshops.common.Constants
import java.util.*

data class Betshop(val name:String,
                   val latitude: Double,
                   val longitude: Double,
                   val id: Long,
                   val county: String,
                   val city: String,
                   val address: String)

fun Betshop.isOpen() : Boolean {
    val currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY)
    return Constants.openinghoursStart <= currentHour  && currentHour < Constants.openinghoursEnd
}

