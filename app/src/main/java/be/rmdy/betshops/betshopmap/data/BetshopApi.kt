package be.rmdy.betshops.betshopmap.data

import be.rmdy.betshops.betshopmap.data.model.BetshopResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface BetshopApi {

    @GET("/betshops")
    suspend fun getBetshops(@Query("boundingBox") boundingBox: String) : Response<BetshopResponse>

}