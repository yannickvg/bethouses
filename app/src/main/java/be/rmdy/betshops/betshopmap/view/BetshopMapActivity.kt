package be.rmdy.betshops.betshopmap.view

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import be.rmdy.betshops.R
import be.rmdy.betshops.common.Constants
import be.rmdy.betshops.databinding.ActivityBetshopMapBinding
import com.google.android.gms.location.LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.CancellationTokenSource
import com.google.maps.android.clustering.ClusterManager
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class BetshopMapActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnCameraIdleListener {

    private lateinit var map: GoogleMap
    private lateinit var binding: ActivityBetshopMapBinding
    private val viewModel: BetshopsViewModel by viewModels()

    private var clusterManager: ClusterManager<ClusteredBetshop>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBetshopMapBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupMapFragment()
        observeBetshops()
        observeSelectedBetshop()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        setupMap()
        setupClustering()
        requestLocation()
        loadBetshops()
    }

    override fun onCameraIdle() {
        loadBetshops()
    }

    private fun setupMap() {
        map.setOnCameraIdleListener(this)
        map.setMinZoomPreference(6.0.toFloat())
    }

    private fun setupMapFragment() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun setupClustering() {
        clusterManager = ClusterManager(this, map)
        clusterManager!!.renderer = MarkerClusterRenderer(this, map, clusterManager)
        clusterManager!!.setOnClusterItemClickListener { selectedCluster ->
            viewModel.selectBetshop(selectedCluster)
            true
        }
    }

    private fun getBoundingBox(): String {
        val currentBounds = map.projection
            .visibleRegion.latLngBounds
        return "${currentBounds.northeast.latitude},${currentBounds.northeast.longitude},${currentBounds.southwest.latitude},${currentBounds.southwest.longitude}"
    }

    private fun loadBetshops() {
        viewModel.loadBetshops(getBoundingBox())
    }

    private fun observeSelectedBetshop() {
        viewModel.getSelectedBetshop().observe(this) { betshop ->
            betshop?.let {
                val bottomSheet = BetshopBottomSheet(it) { viewModel.clearSelectedBetshop() }
                bottomSheet.show(
                    supportFragmentManager,
                    "BetshopBottomSheet"
                )
            }
        }
    }

    private fun observeBetshops() {
        viewModel.getBetshops().observe(this) { betshops ->
            clusterManager?.let { clusterManager ->
                clusterManager.clearItems()
                clusterManager.addItems(betshops)
                clusterManager.cluster()
            }
        }
    }

    private val requestPermissionLauncher =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            putLocationOnMap(isGranted)
        }

    private fun requestLocation() {
        when (PackageManager.PERMISSION_GRANTED) {
            ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) -> {
                putLocationOnMap(true)
            }
            else -> {
                requestPermissionLauncher.launch(Manifest.permission.ACCESS_COARSE_LOCATION)
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun putLocationOnMap(permissionGranted: Boolean) {
        if (permissionGranted) {
            map.isMyLocationEnabled = true
            val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
            val cts = CancellationTokenSource()
            val locationResult = fusedLocationProviderClient.getCurrentLocation(PRIORITY_BALANCED_POWER_ACCURACY, cts.token)
            locationResult.addOnSuccessListener { location: Location? ->
                if (location != null) {
                    map.moveCamera(CameraUpdateFactory.newLatLng(
                        LatLng(location.latitude,
                            location.longitude)))
                } else {
                    map.moveCamera(CameraUpdateFactory.newLatLng(Constants.munichCoordinates))
                }
            }
        } else {
            map.moveCamera(CameraUpdateFactory.newLatLng(Constants.munichCoordinates))
        }
    }

}