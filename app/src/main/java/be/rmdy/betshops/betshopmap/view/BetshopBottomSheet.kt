package be.rmdy.betshops.betshopmap.view

import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.os.Bundle
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import be.rmdy.betshops.R
import android.content.DialogInterface
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import be.rmdy.betshops.betshopmap.model.Betshop
import be.rmdy.betshops.betshopmap.model.isOpen
import be.rmdy.betshops.common.Constants
import android.content.Intent
import android.net.Uri
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.widget.ImageViewCompat


class BetshopBottomSheet(val betshop:Betshop, val onCancelAction: () -> Unit) : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(
            R.layout.betshop_bottom_sheet,
            container, false
        )
        val closeButton: ImageButton = view.findViewById(R.id.betshop_dialog_close)
        closeButton.setOnClickListener {
            onCancelAction()
            dismiss() }

        giveImageViewGreenTint(view.findViewById(R.id.betshop_location_image))
        giveImageViewGreenTint(view.findViewById(R.id.betshop_openinghours_image))

        val detailsLabel: TextView = view.findViewById(R.id.betshop_dialog_details)
        detailsLabel.text = resources.getString(R.string.label_betshop_detail, betshop.name.trim(), betshop.address, betshop.city, betshop.county)

        val openinghoursLabel: TextView = view.findViewById(R.id.betshop_dialog_openinghours)
        openinghoursLabel.text = if (betshop.isOpen()) {resources.getString(R.string.label_open, "${Constants.openinghoursEnd}:00")}
        else { resources.getString(R.string.label_closed, "${Constants.openinghoursStart}:00")}

        val routeButton: Button = view.findViewById(R.id.betshop_button_route)
        routeButton.setOnClickListener {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse("google.navigation:q=${betshop.latitude},${betshop.longitude}")
            )
            startActivity(intent)
        }
        return view
    }

    override fun onCancel(dialog: DialogInterface) {
        onCancelAction()
        super.onCancel(dialog)
    }

    fun giveImageViewGreenTint(imageView: ImageView) {
        ImageViewCompat.setImageTintList(imageView, ContextCompat.getColorStateList(requireContext(), R.color.betshops_green))
    }
}