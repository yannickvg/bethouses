package be.rmdy.betshops.betshopmap.view

import android.content.Context
import be.rmdy.betshops.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer

class MarkerClusterRenderer(context: Context, map: GoogleMap, clusterManager: ClusterManager<ClusteredBetshop>?):
    DefaultClusterRenderer<ClusteredBetshop>(context, map, clusterManager) {

    override fun onBeforeClusterItemRendered(item: ClusteredBetshop, markerOptions: MarkerOptions) {
        super.onBeforeClusterItemRendered(item, markerOptions)
        markerOptions.icon(BitmapDescriptorFactory.fromResource( if (item.isSelected) R.drawable.ic_pin_active else R.drawable.ic_pin_normal))
    }


}