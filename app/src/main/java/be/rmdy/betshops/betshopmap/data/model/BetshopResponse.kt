package be.rmdy.betshops.betshopmap.data.model

data class BetshopResponse(val count: Long, val betshops: List<ApiBetshop>)