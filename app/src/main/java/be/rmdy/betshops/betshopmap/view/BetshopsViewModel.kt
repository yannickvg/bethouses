package be.rmdy.betshops.betshopmap.view

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import be.rmdy.betshops.betshopmap.data.BetshopRepository
import be.rmdy.betshops.betshopmap.model.Betshop
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BetshopsViewModel @Inject constructor(
    application: Application,
    private val betshopRepository: BetshopRepository
) : AndroidViewModel(application) {

    private var betshops: List<Betshop> = emptyList()
    private var selectedBetshop: Betshop? = null

    private val liveDataBetshops: MutableLiveData<List<ClusteredBetshop>> = MutableLiveData<List<ClusteredBetshop>>()
    private val liveDataSelectedBetshop: MutableLiveData<Betshop?> = MutableLiveData<Betshop?>()

    fun getBetshops(): LiveData<List<ClusteredBetshop>> {
        return liveDataBetshops
    }

    fun getSelectedBetshop(): LiveData<Betshop?> {
        return liveDataSelectedBetshop
    }

    fun loadBetshops(boundingBox: String) {
        viewModelScope.launch(Dispatchers.IO) {
            betshops = betshopRepository.getBetshops(boundingBox)
            updateBetshopsLiveData()
        }
    }

    fun selectBetshop(clusteredBetshop: ClusteredBetshop) {
        val matchingBetshop = betshops.find { it.id == clusteredBetshop.getId()}
        selectedBetshop = matchingBetshop
        updateBetshopsLiveData()
        liveDataSelectedBetshop.postValue(matchingBetshop)
    }

    fun clearSelectedBetshop() {
        selectedBetshop = null
        updateBetshopsLiveData()
        liveDataSelectedBetshop.postValue(selectedBetshop)
    }

    private fun updateBetshopsLiveData() {
        liveDataBetshops.postValue(betshops.map {
            ClusteredBetshop(it,
                selectedBetshop?.id == it.id ) })
    }

}