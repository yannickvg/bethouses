package be.rmdy.betshops.common

import com.google.android.gms.maps.model.LatLng

class Constants {
    companion object {
        const val openinghoursStart = 8
        const val openinghoursEnd = 16
        val munichCoordinates = LatLng(48.137154, 11.576124)
    }
}

