package be.rmdy.betshops

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BetshopApplication : Application()