package be.rmdy.betshops.di

import be.rmdy.betshops.betshopmap.data.BetshopApi
import be.rmdy.betshops.betshopmap.data.BetshopRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(gson: GsonConverterFactory) : Retrofit = Retrofit.Builder()
    .baseUrl("https://interview.superology.dev")
    .addConverterFactory(gson)
    .build()

    @Singleton
    @Provides
    fun provideGson(): GsonConverterFactory = GsonConverterFactory.create()

    @Singleton
    @Provides
    fun provideBetshopApi(retrofit: Retrofit): BetshopApi = retrofit.create(BetshopApi::class.java)

    @Provides
    @Singleton
    fun provideRepository(betshopApi: BetshopApi) =
        BetshopRepository(betshopApi)
}